import React from "react";
import UserNav from "./UserNav";

export default function HeaderTheme() {
  return (
    <div>
      <div className="h-20 px-10 flex items-center justify-between shadow-lg">
        <div className="logo text-2xl font-medium">Quyen</div>
        <UserNav />
      </div>
    </div>
  );
}
