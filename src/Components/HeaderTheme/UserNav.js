import React from "react";
import { useSelector } from "react-redux";

export default function UserNav() {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  console.log("userInfor: ", userInfor);
  return <div>{userInfor.hoTen}</div>;
}
