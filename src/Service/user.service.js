import axios from "axios";
import { TOKEN_CYBER } from "./configURL";

export let userService = {
  postLogin: (loginData) => {
    return axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
      method: "POST",
      data: loginData,
      headers: {
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },
};
