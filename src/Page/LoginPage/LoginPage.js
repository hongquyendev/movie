import React from "react";
import { Form, Input, message } from "antd";
import { userService } from "../../Service/user.service";
import { localStorageServ } from "../../Service/localStorageService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { loginAction } from "../Redux/Actions/userActions";

export default function LoginPage() {
  let dispatch = useDispatch();
  let history = useNavigate();
  const onFinish = (values) => {
    console.log("Success:", values);
    userService
      .postLogin(values)
      .then((res) => {
        // console.log("res: ", res);
        message.success("Đăng nhập thành công");
        dispatch(loginAction(res.data.content));
        localStorageServ.user.set(res.data.content);
        // window.location.href = "./";
        setTimeout(() => {
          history("/");
        }, 1000);
      })
      .catch((err) => {
        // console.log("err: ", err.response.data.content);
        message.error(err.response.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="bg-red-400 h-screen w-screen p-10">
      <div className="container bg-white mx-auto rounded-xl p-10">
        <Form
          name="basic"
          layout="vertical"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 24,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Tài khoản"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tài khoản!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật khẩu"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập mật khẩu!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <div className="flex justify-center">
            <button className="inline-flex items-center justify-center px-8 py-4 font-sans font-semibold tracking-wide text-white bg-blue-500 rounded-lg h-[60px]">
              Đăng nhập
            </button>
          </div>
        </Form>
      </div>
    </div>
  );
}
