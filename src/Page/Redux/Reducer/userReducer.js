import { localStorageServ } from "../../../Service/localStorageService";

let initialState = {
  userInfo: localStorageServ.user.get(),
};

export let userReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LOGIN": {
      state.userInfo = action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
